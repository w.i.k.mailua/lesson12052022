# Оголосіть функцію з ім'ям , яка приймає два аргументи (два числа) і ще один формальний
# параметр type з початковим значенням str. Якщо параметр type дорівнює нулю, то функція має повертати
# периметр прямокутника, а інакше – його площу.


def get_rectangle_info(a, b, *, type='hjgf'):
    """
    Args:
        a (int|float):
        b (int|float):
        type (str):
    Returns:
        square, perimetr, diagonal or None
    """

    rules = {
        's': lambda: a * b,
        'p': lambda: (a + b) * 2,
        'd': lambda: (a ** 2 + b ** 2) ** 0.5,
    }

    if rules.get(type):
        return rules.get(type)()


print(get_rectangle_info(3, 4, type='s'))

#

# from datetime import datetime
# from time import sleep
#
#
# def time_now(msg):
#     print(msg, datetime.now())
#
#
# sleep(1)
# time_now('try')
# sleep(1)
# time_now('try2')
# sleep(1)
# time_now('try3')
# sleep(1)

#
# скобки (одного типу і багатьох)

ex1 = '(5+5)/4*(5+9)'
ex2 = '(5+[(5)/4*5+9)]<>'


def check_brackets(text, brackets='<>()[]{}'):
    opening, closing = brackets[::2], brackets[1::2]
    stack = []
    for char in text:
        if char in opening:
            stack.append(opening.index(char))
        elif char in closing:
            if stack and stack[-1] == closing.index(char):
                stack.pop()
            else:
                return False
    return not stack


print(check_brackets(ex1))
print(check_brackets(ex2))
